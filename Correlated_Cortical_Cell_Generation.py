# -*- coding: utf-8 -*-
"""
Created on Fri Jun 21 18:11:08 2019

This library generates the correlated inputs for the motoneuron pool using a 
an integrate and fire pulse encoder that generates weakly beta-modulated spike 
trains. The spike trains are pre-convolved with an EPSP before they are called 
and loaded into the IClamp function within the main run file.

The percent connectivity of the network is pre-loaded into a dictionary and
used to establish the number of motoneuron input connections.

@author: Sageanne Senneff
"""
import numpy as np
import scipy.io as sio
import random

def Pulse_Encoder(self, A, B, total_no_input, cortical_sig, durn, dt, start, endtime, EPSP_CT, EPSP_CT_2, pulse_train, pulse_train_total):
   
    ## Pulse Encoder Parameters
    ZMAX    = 1.0                                                              # Threshold at which ENCODER fires
    GAIN    = 1.0                                                              # Gain of the ENCODER
    H       = dt                                                               # Sampling Rate
    TAU     = 0.025                                                            # ENCODER time constant (Reduced value to increase encoder synchrony)
    coeff01 = TAU/(H+TAU)
    coeff02 = GAIN*H/(H+TAU)

    ## Initialize Encoder Variables
    znow = np.zeros([total_no_input,1])
    tnow = np.zeros([total_no_input,1]) 
    zold = np.zeros([total_no_input,1])
    start_pe = np.ones([total_no_input,1])
    nspike = np.ones([total_no_input,1])

    ## Generate Spike Trains
    for t in range(int(start), int(endtime)):
        for no_input in range(total_no_input):
            if (znow[no_input,0] < ZMAX):                                          # ENCODER below threshold
                zold[no_input,0] = znow[no_input,0]
                start_pe = 1
                if start_pe:
                    v1 = 2.0*random.uniform(0,1)-1.0
                    v2 = 2.0*random.uniform(0,1)-1.0
                    w = v1*v1 + v2*v2 
                    w = np.log(w)/w
                    if np.sign(w) == -1:
                        w = np.sqrt(-w-w)
                    else:
                        w = np.sqrt(w+w)
                    g1 = v1*w
                    g2 = v2*w
                    start = 0
                    normal_output = A + B*g1
                else:
                    start_pe = 1
                    normal_output = A + B*g2                   
                znow[no_input,0] = coeff01*znow[no_input,0] + coeff02*normal_output + cortical_sig[0,t-1]
                tnow[no_input,0] = tnow[no_input,0] + H
                pulse_train[t,no_input] = 0
            elif (znow[no_input,0] == ZMAX):                                       # ENCODER on threshold
                tnow[no_input,0] = 0
                znow[no_input,0] = 0
                zold[no_input,0] = 0
                nspike[no_input,0] = nspike[no_input,0] + 1
                pulse_train[t,no_input] = 1
                pulse_train_total[no_input,t:t+len(EPSP_CT_2)] = (pulse_train_total[no_input,t:t+len(EPSP_CT_2)])+(EPSP_CT_2*pulse_train[t,no_input])
            else:                                                                  # ENCODER above threshold
                tmp = H*(znow[no_input,0]-ZMAX)/(znow[no_input,0]-zold[no_input,0])
                nspike[no_input,0]=nspike[no_input,0]+1
                if znow[no_input,0] == 0:
                    znow[no_input,0] = 0
                else:
                    znow[no_input,0] = znow[no_input,0]%ZMAX + (ZMAX*(np.sign(znow[no_input,0]) - 1)/2)
                zold[no_input,0] = znow[no_input,0]
                tnow[no_input,0] = tmp
                pulse_train[t,no_input] = 1
                pulse_train_total[no_input,t:t+len(EPSP_CT_2)] = (pulse_train_total[no_input,t:t+len(EPSP_CT_2)])+(EPSP_CT_2*pulse_train[t,no_input])  
                
    
   # myPulseTrains_Correlated = np.save('Correlated_Pulse_Trains.mat', {'pulse_train_correlated': pulse_train})
    
    return pulse_train_total, pulse_train#, myPulseTrains_Correlated
    
def Pulse_Encoder_Update(self, A, B, total_no_input, cortical_sig, durn, dt, start, endtime, EPSP_CT, EPSP_CT_2, controller_call_time):
   
    ## Pulse Encoder Parameters
    ZMAX    = 1.0                                                              # Threshold at which ENCODER fires
    GAIN    = 1.0                                                              # Gain of the ENCODER
    H       = dt                                                               # Sampling Rate
    TAU     = 0.025                                                            # ENCODER time constant (Reduced value to increase encoder synchrony)
    coeff01 = TAU/(H+TAU)
    coeff02 = GAIN*H/(H+TAU)

    ## Initialize Encoder Variables
    znow = np.zeros([total_no_input,1])
    tnow = np.zeros([total_no_input,1]) 
    zold = np.zeros([total_no_input,1])
    start_pe = np.ones([total_no_input,1])
    nspike = np.ones([total_no_input,1])
    pulse_train = np.zeros([len(np.arange(0, self.durn + self.dt, self.dt)), total_no_input])
    pulse_train_total = np.zeros([total_no_input, len(np.arange(0, self.durn + self.dt, self.dt)) + len(self.EPSP_CT)])

    ## Generate Spike Trains
    for t in range(int(endtime)):
        for no_input in range(total_no_input):
            if (znow[no_input,0] < ZMAX):                                          # ENCODER below threshold
                zold[no_input,0] = znow[no_input,0]
                start_pe = 1
                if start_pe:
                    v1 = 2.0*random.uniform(0,1)-1.0
                    v2 = 2.0*random.uniform(0,1)-1.0
                    w = v1*v1 + v2*v2 
                    w = np.log(w)/w
                    if np.sign(w) == -1:
                        w = np.sqrt(-w-w)
                    else:
                        w = np.sqrt(w+w)
                    g1 = v1*w
                    g2 = v2*w
                    start_pe = 0
                    normal_output = A + B*g1
                else:
                    start_pe = 1
                    normal_output = A + B*g2                   
                k = int(controller_call_time + t)
                znow[no_input,0] = coeff01*znow[no_input,0] + coeff02*normal_output + cortical_sig[0,k-1]
                tnow[no_input,0] = tnow[no_input,0] + H
                pulse_train[t,no_input] = 0
            elif (znow[no_input,0] == ZMAX):                                       # ENCODER on threshold
                tnow[no_input,0] = 0
                znow[no_input,0] = 0
                zold[no_input,0] = 0
                nspike[no_input,0] = nspike[no_input,0] + 1
                pulse_train[t,no_input] = 1
                pulse_train_total[no_input,t:t+len(EPSP_CT_2)] = (pulse_train_total[no_input,t:t+len(EPSP_CT_2)])+(EPSP_CT_2*pulse_train[t,no_input])
            else:                                                                  # ENCODER above threshold
                tmp = H*(znow[no_input,0]-ZMAX)/(znow[no_input,0]-zold[no_input,0])
                nspike[no_input,0]=nspike[no_input,0]+1
                if znow[no_input,0] == 0:
                    znow[no_input,0] = 0
                else:
                    znow[no_input,0] = znow[no_input,0]%ZMAX + (ZMAX*(np.sign(znow[no_input,0]) - 1)/2)
                zold[no_input,0] = znow[no_input,0]
                tnow[no_input,0] = tmp
                pulse_train[t,no_input] = 1
                pulse_train_total[no_input,t:t+len(EPSP_CT_2)] = (pulse_train_total[no_input,t:t+len(EPSP_CT_2)])+(EPSP_CT_2*pulse_train[t,no_input])  

    return pulse_train_total, pulse_train

        
def Correlated_Cell_Generation(self, no_mu, filepath, durn, dt, A, B, cortical_sig_amp, pk, start, endtime, EPSP_CT, EPSP_CT_2, pulse_train, pulse_train_total, temp_pulse_CT):

    ## Connectivity Parameters
    Connectivity_Dict = sio.loadmat('InputConnectionDirect_15pc_600_100.mat')
    InputConnection = np.array(Connectivity_Dict['InputConnection'])
    total_no_input = np.amax(InputConnection)                                  # total number of correlated inputs to each MN 
    
    ## Initialize Cortical Signals
    Beta_Dict = sio.loadmat('cortical_sig_2000_011019_A01.mat')    
    cortical_sig_temp = np.array(Beta_Dict['cortical_sig_temp'])
    cortical_sig = np.zeros([total_no_input, np.size(cortical_sig_temp[1])])    
    NoiseRatioCT = 1
    if (pk==0.1):
        for ix in range(1,2):
            cortical_sig = cortical_sig_temp[ix,:]/np.std(cortical_sig_temp[ix,:])*cortical_sig_amp                                    
            cortical_sig = np.tile(cortical_sig, [total_no_input,1])
        for ix in range(0,np.size(cortical_sig,0)):
            cortical_sig[ix,:] = cortical_sig[ix,:] + np.random.uniform(low = 0, high = 1, size = np.size(cortical_sig_temp[1]))*np.std(cortical_sig[ix,:])*(NoiseRatioCT*0.75)
            cortical_sig[ix,:] = cortical_sig[ix,:]/np.std(cortical_sig[ix,:])*cortical_sig_amp
    else:                        
        for ix in range(1,2):
            cortical_sig = cortical_sig_temp[ix,:]/np.std(cortical_sig_temp[ix,:])*cortical_sig_amp
        cortical_sig = np.tile(cortical_sig, [total_no_input,1])  
        for ix in range(0,np.size(cortical_sig,0)): 
            cortical_sig[ix,:] = cortical_sig[ix,:] + np.random.uniform(low = 0, high = 1, size = np.size(cortical_sig_temp[1]))*np.std(cortical_sig[ix,:])*NoiseRatioCT
            cortical_sig[ix,:] = cortical_sig[ix,:]/np.std(cortical_sig[ix,:])*cortical_sig_amp  
 
    #pulse_train_total, pulse_train, myPulseTrains_Correlated = Pulse_Encoder(self, A, B, total_no_input, cortical_sig, durn, dt, start, endtime, EPSP_CT, EPSP_CT_2, pulse_train, pulse_train_total)
    pulse_train_total, pulse_train = Pulse_Encoder(self, A, B, total_no_input, cortical_sig, durn, dt, start, endtime, EPSP_CT, EPSP_CT_2, pulse_train, pulse_train_total)

    for t in range(int(start), int(endtime)):
        for i in range(self.no_mu):
            temp_pulse_CT[i,t] = sum(pulse_train_total[InputConnection[:, i]-1,t])
    
    return temp_pulse_CT, pulse_train_total, pulse_train
    
def Correlated_Cell_Generation_Update(self, no_mu, filepath, durn, dt, A, B, cortical_sig_amp, pk, start, endtime, EPSP_CT, EPSP_CT_2, controller_call_time):

    ## Connectivity Parameters
    Connectivity_Dict = sio.loadmat('InputConnectionDirect_15pc_600_100.mat')
    InputConnection = np.array(Connectivity_Dict['InputConnection'])
    total_no_input = np.amax(InputConnection)                                  # total number of correlated inputs to each MN 
    
    ## Initialize Cortical Signals
    Beta_Dict = sio.loadmat('cortical_sig_2000_011019_A01.mat')    
    cortical_sig_temp = np.array(Beta_Dict['cortical_sig_temp'])
    cortical_sig = np.zeros([total_no_input, np.size(cortical_sig_temp[1])])    
    NoiseRatioCT = 1
    if (pk==0.1):
        for ix in range(1,2):
            cortical_sig = cortical_sig_temp[ix,:]/np.std(cortical_sig_temp[ix,:])*cortical_sig_amp                                    
            cortical_sig = np.tile(cortical_sig, [total_no_input,1])
        for ix in range(0,np.size(cortical_sig,0)):
            cortical_sig[ix,:] = cortical_sig[ix,:] + np.random.uniform(low = 0, high = 1, size = np.size(cortical_sig_temp[1]))*np.std(cortical_sig[ix,:])*(NoiseRatioCT*0.75)
            cortical_sig[ix,:] = cortical_sig[ix,:]/np.std(cortical_sig[ix,:])*cortical_sig_amp
    else:                        
        for ix in range(1,2):
            cortical_sig = cortical_sig_temp[ix,:]/np.std(cortical_sig_temp[ix,:])*cortical_sig_amp
        cortical_sig = np.tile(cortical_sig, [total_no_input,1])  
        for ix in range(0,np.size(cortical_sig,0)): 
            cortical_sig[ix,:] = cortical_sig[ix,:] + np.random.uniform(low = 0, high = 1, size = np.size(cortical_sig_temp[1]))*np.std(cortical_sig[ix,:])*NoiseRatioCT
            cortical_sig[ix,:] = cortical_sig[ix,:]/np.std(cortical_sig[ix,:])*cortical_sig_amp  
 
    pulse_train_total, pulse_train = Pulse_Encoder_Update(self, A, B, total_no_input, cortical_sig, durn, dt, start, endtime, EPSP_CT, EPSP_CT_2, controller_call_time)
    
    temp_pulse_CT = np.zeros([self.no_mu, len(np.arange(0, self.durn + self.dt, self.dt))])                 # Common Modulatory Pulse Train
    for t in range(int(endtime)):
        for i in range(self.no_mu):
            temp_pulse_CT[i,t] = sum(pulse_train_total[InputConnection[:, i]-1,t])

    return temp_pulse_CT



