# -*- coding: utf-8 -*-
"""
Created on Fri Jun 21 12:37:25 2019

This library generates both the excitatory and the inhibitory independent inputs 
for the motoneuron pool using a Poisson process with renewal. The pulse trains
are pre-convolved with an EPSP before they are called and loaded into the
IClamp function within the main run file.

@author: Sageanne Senneff
"""

import numpy as np
import random
import scipy.io as sio
           
def Excitatory_Cell_Generation(self, k, fr2, dt, no_mu, total_no_input_ind, durn, max_syn, a_s, start, endtime, EPSP, pulse_train_ind, pulse_train_ind_ip, pulse_count_ind, temp_pulse_ex):
   
    ## Generate a 30 second signal
    n = int(start)
    for t in range(int(start), int(endtime)):
        
        n = n + 1
        temp1 = 0.1 + np.random.uniform(low = 0.1, high = 0.75, size = (total_no_input_ind, max_syn))

        # Calculate Independent Inputs at each Time Step
        for m in range(no_mu):
            for no_input in range(total_no_input_ind):
                pspike = fr2*dt*k
                pulse_train_ind[m,no_input,n] = (pspike > random.uniform(0, 1))# Fire with Poisson Distribution (renewal process)
                if pulse_train_ind[m,no_input,n] == 1:                         # Remove every kth Firing Time
                    pulse_count_ind[m,no_input] = pulse_count_ind[m,no_input] + 1
                    if pulse_count_ind[m,no_input]%k == 0:
                        pulse_train_ind[m,no_input,n] = 0
                    else:
                        pulse_train_ind[m,no_input,n] = sum(sum((temp1[no_input,:] > np.random.uniform(low = 0, high = 1, size = (1, max_syn)))))*a_s
                        pulse_train_ind_ip[m,no_input,n:n+len(EPSP)] = np.squeeze(pulse_train_ind_ip[m,no_input,n:n+len(EPSP)]) + (np.transpose(EPSP)*pulse_train_ind[m,no_input,n]) 
            temp_pulse_ex[m,n] = sum(pulse_train_ind_ip[m,:,n])  
            
    #myPulseTrains_Excitatory = np.save('Excitatory_Pulse_Trains.mat', {'pulse_train_excitatory': pulse_train_ind})

    return temp_pulse_ex, pulse_train_ind, pulse_count_ind, pulse_train_ind_ip#, myPulseTrains_Excitatory
    
def Excitatory_Cell_Generation_Update(self, k, fr2, dt, no_mu, total_no_input_ind, durn, max_syn, a_s, start, endtime, EPSP):
   
    pulse_train_ind = np.zeros([self.no_mu, self.total_no_input_ind, len(np.arange(0, self.durn + self.dt, self.dt))])
    pulse_train_ind_ip = np.zeros([self.no_mu, self.total_no_input_ind, len(np.arange(0, self.durn + self.dt, self.dt)) + len(self.EPSP)])
    pulse_count_ind = np.zeros([self.no_mu, self.total_no_input_ind]) 
    temp_pulse_ex = np.zeros([self.no_mu, len(np.arange(0, self.durn + self.dt, self.dt))])

    for t in range(int(endtime)):
        
        temp1 = 0.1 + np.random.uniform(low = 0.1, high = 0.75, size = (total_no_input_ind, max_syn))

        # Calculate Independent Inputs at each Time Step
        for m in range(no_mu):
            for no_input in range(total_no_input_ind):
                pspike = fr2*dt*k
                pulse_train_ind[m,no_input,t] = (pspike > random.uniform(0, 1))# Fire with Poisson Distribution (renewal process)
                if pulse_train_ind[m,no_input,t] == 1:                         # Remove every kth Firing Time
                    pulse_count_ind[m,no_input] = pulse_count_ind[m,no_input] + 1
                    if pulse_count_ind[m,no_input]%k == 0:
                        pulse_train_ind[m,no_input,t] = 0
                    else:
                        pulse_train_ind[m,no_input,t] = sum(sum((temp1[no_input,:] > np.random.uniform(low = 0, high = 1, size = (1, max_syn)))))*a_s
                        pulse_train_ind_ip[m,no_input,t:t+len(EPSP)] = np.squeeze(pulse_train_ind_ip[m,no_input,t:t+len(EPSP)]) + (np.transpose(EPSP)*pulse_train_ind[m,no_input,t]) 
            temp_pulse_ex[m,t] = sum(pulse_train_ind_ip[m,:,t])  
        
    return temp_pulse_ex


def Inhibitory_Cell_Generation(self, k, fr2, dt, no_mu, total_no_input_inhib, durn, max_syn, a_s_inhib, start, endtime, EPSP, pulse_train_inhib, pulse_train_inhib_ip, pulse_count_inhib, temp_pulse_inhib):
    
    ## Generate a 30 second signal
    n = int(start)
    for t in range(int(start), int(endtime)):
        
        n = n + 1
        temp2 = 0.1 + np.random.uniform(low = 0.1, high = 0.75, size = (total_no_input_inhib, max_syn))
    
        # Calculate Independent Inputs at each Time Step
        for m in range(no_mu):
            for no_input in range(total_no_input_inhib):
                pspike = fr2*dt*k
                pulse_train_inhib[m,no_input,n]=(pspike > random.uniform(0, 1)) # Fire with Poisson Distribution (renewal process)
                if pulse_train_inhib[m,no_input,n]==1: # Remove every kth Firing Time
                    pulse_count_inhib[m,no_input] = pulse_count_inhib[m,no_input] + 1
                    if pulse_count_inhib[m,no_input]%k == 0:
                        pulse_train_inhib[m,no_input,n] = 0
                    else:
                        pulse_train_inhib[m,no_input,n] = sum(sum((temp2[no_input,:] > np.random.uniform(low = 0, high = 1, size = (1, max_syn)))))*a_s_inhib
                        pulse_train_inhib_ip[m,no_input,n:n+len(EPSP)] = np.squeeze(pulse_train_inhib_ip[m,no_input,n:n+len(EPSP)])+(np.transpose(EPSP)*pulse_train_inhib[m,no_input,n])            
            temp_pulse_inhib[m,n] = sum(pulse_train_inhib_ip[m,:,n]) # Drive with alpha pulse trains
         
    #myPulseTrains_Inhibitory = np.save('Inhibitory_Pulse_Trains.mat', {'pulse_train_excitatory': pulse_train_inhib})
       
    return temp_pulse_inhib, pulse_train_inhib, pulse_count_inhib, pulse_train_inhib_ip#, myPulseTrains_Inhibitory
    
def Inhibitory_Cell_Generation_Update(self, k, fr2, dt, no_mu, total_no_input_inhib, durn, max_syn, a_s_inhib, start, endtime, EPSP):
    
    pulse_train_inhib = np.zeros([self.no_mu, self.total_no_input_inhib, len(np.arange(0, self.durn + self.dt, self.dt))])
    pulse_train_inhib_ip = np.zeros([self.no_mu, self.total_no_input_inhib, len(np.arange(0, self.durn + self.dt, self.dt)) + len(self.EPSP)])
    pulse_count_inhib = np.zeros([self.no_mu, self.total_no_input_inhib]) 
    temp_pulse_inhib = np.zeros([self.no_mu, len(np.arange(0, self.durn + self.dt, self.dt))])
    
    for t in range(int(endtime)):
        
        temp2 = 0.1 + np.random.uniform(low = 0.1, high = 0.75, size = (total_no_input_inhib, max_syn))
    
        # Calculate Independent Inputs at each Time Step
        for m in range(no_mu):
            for no_input in range(total_no_input_inhib):
                pspike = fr2*dt*k
                pulse_train_inhib[m,no_input,t]=(pspike > random.uniform(0, 1)) # Fire with Poisson Distribution (renewal process)
                if pulse_train_inhib[m,no_input,t]==1: # Remove every kth Firing Time
                    pulse_count_inhib[m,no_input] = pulse_count_inhib[m,no_input] + 1
                    if pulse_count_inhib[m,no_input]%k == 0:
                        pulse_train_inhib[m,no_input,t] = 0
                    else:
                        pulse_train_inhib[m,no_input,t] = sum(sum((temp2[no_input,:] > np.random.uniform(low = 0, high = 1, size = (1, max_syn)))))*a_s_inhib
                        pulse_train_inhib_ip[m,no_input,t:t+len(EPSP)] = np.squeeze(pulse_train_inhib_ip[m,no_input,t:t+len(EPSP)])+(np.transpose(EPSP)*pulse_train_inhib[m,no_input,t])            
            temp_pulse_inhib[m,t] = sum(pulse_train_inhib_ip[m,:,t]) # Drive with alpha pulse trains
            
    return temp_pulse_inhib


