# -*- coding: utf-8 -*-
"""
Created on Mon Jan 27 14:40:24 2020

@author: Sageanne
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Jun 20 20:05:32 2019

This is the main run file for the integrated cortical network, motoneuron pool, 
and force twitch model. The cortical signals are generated in libraries
"Correlated_Cortical_Cell_Generation", "Excitatory_Cell_Generation", and
"Inhibitory_Cell_Generation". Model parameters for the motoneuron pool are set
in the library "Gather_Parameters". NEURON is used as a simulation platform to
simulate a pool of 100 motoneurons with a five-compartment structure. 
A proportional force controller adjusts the firing rate of the cortical network
model until a target force (% MVC) is reached. 

@author: Sageanne Senneff
"""
from neuron import h
from Cortical_Cell_Generation import Excitatory_Cell_Generation, Excitatory_Cell_Generation_Update, Inhibitory_Cell_Generation, Inhibitory_Cell_Generation_Update
from Gather_Parameters import Set_Parameters_Soma, Set_Parameters_D1, Set_Parameters_D2, Set_Parameters_D3, Set_Parameters_D4
from Correlated_Cortical_Cell_Generation import Correlated_Cell_Generation, Correlated_Cell_Generation_Update
from Gather_Distributions import Get_P_T_Distributions

import numpy as np
from scipy.stats import norm, weibull_min
import scipy.io as sio
from matplotlib import pyplot as pp
import sys

class Run_Neuromuscular_Model():
    
    ## Load Necessary Neuron Libraries                             
    h.load_file("stdrun.hoc")                                                     
    h.load_file("stdlib.hoc")    
    
    def __init__(self):
        
        self.dt = 0.001
        self.no_mu = 100
        self.k = 2.0
        self.filepath = 'C:/Users/Sageanne/Desktop/Powers Model/motoneuron_python_model' # Signal File Path
        self.pk = 0.2#float(sys.argv[2])
        self.ss_length = 2.0                                                   # Length of steady state force trajectory
        self.durn = ((0.75*self.pk*10)*2) + self.ss_length
        self.Fmax = 1.6798e+04#7.4558e+03 
        
        h.celsius = 37.0                                                       # Temperature
        h.tstop = (0.75*self.pk*10)/self.dt#int(self.durn/self.dt)                                       # Simulation Duration
        h.dt = 0.05                                                            # Sampling Rate of Pool Model
        self.CaPIC = 1.0
        
        if self.pk == 1.0:
            self.durn = 5.0
            h.tstop = 5000
        
        ## Set Cortical Network Parameters
        self.A = 15.0                                                           # Mean White Noise (Input Rate)
        self.fr2 = self.A*3.1
        self.fr = self.A
        self.fr_pi = 15.0
        self.fr2_pi = self.fr_pi*3.1
        self.B = 0.307                                                         # Std White Noise (Input Randomness)
        self.sf = 18.0
        self.scaling_factor = self.sf*2.4e6
        self.max_fr = 200
        self.max_fr2 = self.max_fr*3.1                                         
        
        if self.pk == 1.0:
            self.A = self.max_fr
            self.fr2 = self.max_fr2
        
        self.total_no_input_ind = 75                                           # Number of Excitatory Independent Inputs/MU
        self.total_no_input_inhib = 25                                         # Number of Inhibitory Independent Inputs/M
        self.max_syn = 10                                                      # Synaptic Boutons 
        self.a_s = 68e-10                                                      # Amplitude of Excitatory EPSP                                                     
        self.a_s_inhib = (self.a_s/6)*5                                        # Amplitude of Inhibitory EPSP                                                 
        self.a_ct = 110e-10                                                    # Amplitude of Correlated EPSP
        
        self.tmax_CI = 25                                                      # Time Constant Correlated EPSP
        self.tmax_II = 20                                                      # Time Constant Independent EPSP
        self.tau_CI = 3                                                        # Rise Time Correlated EPSP
        self.tau_II = 2                                                        # Rise Time Independent EPSP
        self.ts = 2                                                            # Activation Time of Spike
        self.d_n = 0.58                                                        # Duration of EPSP
        self.EPSP_dt = 1                                                       # Sampling Rate of EPSP
        self.cortical_sig_amp = 0.01                                           # Amplitude of Cortical Signal
        self.E_e = -70
        self.E_i = -75
 
        ## Set Force Trajectory Parameters
        self.endt = int(0.5/self.dt)                                           # Twitch Duration     

        ## Initialize Cortical Signal Generation Call Times
        self.start = 0                                                         # Start Time
        self.endtime = self.durn/self.dt                                       # End Time
       
    def __create__(self):

        # Initialize Signal Objects         
        h('objref iclamp_d1[100], iclamp_d2[100], iclamp_d3[100], iclamp_d4[100]') 
        h('objref cortical_signals_d1[100], cortical_signals_d2[100], cortical_signals_d3[100], cortical_signals_d4[100]')
        h('objref tvec_d1[100], tvec_d2[100], tvec_d3[100], tvec_d4[100]')
        
        ## Set up Soma Section
        self.all_soma = list() 
        self.all_apc = list()
        for i in range(self.no_mu):
            
            # Insert Conductances
            soma = h.Section()
            soma.insert('na3rp')
            soma.insert('naps')
            soma.insert('kdrRL')
            soma.insert('mAHP')
            soma.insert('gh') 
            soma.insert('pas') 
            
            # Count Spikes
            apc = h.APCount(soma(0.5))
            apc.thresh = -20.0

            # Load Parameters for each MU
            soma.diam, soma.L, soma.Ra, soma.cm, soma.gbar_na3rp, soma.gbar_naps, soma.gMax_kdrRL, soma.gcamax_mAHP, soma.gkcamax_mAHP, soma.ghbar_gh, \
            soma.g_pas, soma.ek, soma.e_pas, soma.taur_mAHP, soma.mtauca_mAHP, soma.sh_na3rp, soma.sh_naps,soma.ar_na3rp, soma.ar_naps, soma.half_gh, \
            soma.vslope_naps, soma.asvh_naps, soma.bsvh_naps, soma.mvhalfca_mAHP, soma.htau_gh, soma.tmin_kdrRL, soma.taumax_kdrRL, soma.qinf_na3rp, \
            soma.thinf_na3rp, soma.mVh_kdrRL = Set_Parameters_Soma(self, self.no_mu, self.filepath, self.CaPIC, i)     
                        
            self.all_apc.append(apc)
            self.all_soma.append(soma)          
        
        ## Set up Dendritic Compartment #1
        self.all_d1 = list()
        self.updated_cortical_signals_d1 = list()
        for i in range(self.no_mu): 
            
            # Insert Conductances
            d1 = h.Section()
            d1.insert('pas')  
            d1.insert('L_Ca_inact')
            d1.insert('gh')
            
            # Connect to Soma Compartment
            d1.connect(self.all_soma[i](1),0)     
            
            # Load Parameters for each MU
            d1.diam, d1.L, d1.Ra, d1.cm, d1.gcabar_L_Ca_inact, d1.ghbar_gh, d1.g_pas, d1.e_pas, d1.half_gh, d1.htau_gh, d1.theta_m_L_Ca_inact, \
            d1.tau_m_L_Ca_inact, d1.theta_h_L_Ca_inact, d1.tau_h_L_Ca_inact, d1.kappa_h_L_Ca_inact = Set_Parameters_D1(self, self.no_mu, self.filepath, self.CaPIC, i) 
            
            # Load Cortical Signals
            h.cortical_signals_d1[i] = h.Vector(h.tstop)
            h.tvec_d1[i] = h.Vector(h.tstop) 
            for t in range(int(h.tstop)):
                h.cortical_signals_d1[i].x[t] = self.scaling_factor*(1/((np.pi*d1.diam*d1.L)*0.01))*(self.all_excitatory[i,t]*(d1(0.5).v - self.E_e) + self.all_inhibitory[i,t]*(d1(0.5).v - self.E_i) + self.all_correlated[i,t]*(d1(0.5).v - self.E_e))
                h.tvec_d1[i].x[t] = t
            h.iclamp_d1[i] = h.IClamp(d1(0.5))
            h.iclamp_d1[i].dur = h.tstop
            h.iclamp_d1[i].delay = 0
            h.cortical_signals_d1[i].play(h.iclamp_d1[i], h.iclamp_d1[i]._ref_amp, h.tvec_d1[i], 1)
            self.updated_cortical_signals_d1.append(h.cortical_signals_d1[i].as_numpy())  

            self.all_d1.append(d1)    

        ## Set up Dendritic Compartment #2
        self.all_d2 = list()
        self.updated_cortical_signals_d2 = list()
        for i in range(self.no_mu): 
 
            # Insert Conductances
            d2 = h.Section()
            d2.insert('pas')  
            d2.insert('L_Ca_inact')
            d2.insert('gh')
            
            # Connect to Soma Compartment
            d2.connect(self.all_soma[i](1),0)
            
            # Load Parameters for each MU
            d2.diam, d2.L, d2.Ra, d2.cm, d2.gcabar_L_Ca_inact, d2.ghbar_gh, d2.g_pas, d2.e_pas, d2.half_gh, d2.htau_gh, d2.theta_m_L_Ca_inact, \
            d2.tau_m_L_Ca_inact, d2.theta_h_L_Ca_inact, d2.tau_h_L_Ca_inact, d2.kappa_h_L_Ca_inact = Set_Parameters_D2(self, self.no_mu, self.filepath, self.CaPIC, i) 
            
            # Load Cortical Signals
            h.cortical_signals_d2[i] = h.Vector(h.tstop)
            h.tvec_d2[i] = h.Vector(h.tstop) 
            for t in range(int(h.tstop)):
                h.cortical_signals_d2[i].x[t] = self.scaling_factor*(1/((np.pi*d2.diam*d2.L)*0.01))*(self.all_excitatory[i,t]*(d2(0.5).v - self.E_e) + self.all_inhibitory[i,t]*(d2(0.5).v - self.E_i) + self.all_correlated[i,t]*(d2(0.5).v - self.E_e))
                h.tvec_d2[i].x[t] = t
            h.iclamp_d2[i] = h.IClamp(d2(0.5))
            h.iclamp_d2[i].dur = h.tstop
            h.iclamp_d2[i].delay = 0
            h.cortical_signals_d2[i].play(h.iclamp_d2[i], h.iclamp_d2[i]._ref_amp, h.tvec_d2[i], 1)
            self.updated_cortical_signals_d2.append(h.cortical_signals_d2[i].as_numpy())  

            self.all_d2.append(d2)    

        ## Set up Dendritic Compartment #3
        self.all_d3 = list()    
        self.updated_cortical_signals_d3 = list()
        for i in range(self.no_mu): 
            
            # Insert Conductances
            d3 = h.Section()
            d3.insert('pas')  
            d3.insert('L_Ca_inact')
            d3.insert('gh')
            
            # Connect to Soma Compartment
            d3.connect(self.all_soma[i](0),0)
                        
            # Load Parameters for each MU
            d3.diam, d3.L, d3.Ra, d3.cm, d3.gcabar_L_Ca_inact, d3.ghbar_gh, d3.g_pas, d3.e_pas, d3.half_gh, d3.htau_gh, d3.theta_m_L_Ca_inact, \
            d3.tau_m_L_Ca_inact, d3.theta_h_L_Ca_inact, d3.tau_h_L_Ca_inact, d3.kappa_h_L_Ca_inact = Set_Parameters_D3(self, self.no_mu, self.filepath, self.CaPIC, i) 

            # Load Cortical Signals
            h.cortical_signals_d3[i] = h.Vector(h.tstop)
            h.tvec_d3[i] = h.Vector(h.tstop) 
            for t in range(int(h.tstop)):
                h.cortical_signals_d3[i].x[t] = self.scaling_factor*(1/((np.pi*d3.diam*d3.L)*0.01))*(self.all_excitatory[i,t]*(d3(0.5).v - self.E_e) + self.all_inhibitory[i,t]*(d3(0.5).v - self.E_i) + self.all_correlated[i,t]*(d3(0.5).v - self.E_e))
                h.tvec_d3[i].x[t] = t
            h.iclamp_d3[i] = h.IClamp(d3(0.5))
            h.iclamp_d3[i].dur = h.tstop
            h.iclamp_d3[i].delay = 0
            h.cortical_signals_d3[i].play(h.iclamp_d3[i], h.iclamp_d3[i]._ref_amp, h.tvec_d3[i], 1)
            self.updated_cortical_signals_d3.append(h.cortical_signals_d3[i].as_numpy())  

            self.all_d3.append(d3)    
        
        ## Set up Dendritic Compartment #4
        self.all_d4 = list()
        self.updated_cortical_signals_d4 = list()
        for i in range(self.no_mu): 
            
            # Insert Conductances
            d4 = h.Section()
            d4.insert('pas')  
            d4.insert('L_Ca_inact')
            d4.insert('gh')
            
            # Connect to Soma Compartment
            d4.connect(self.all_soma[i](0),0)    
            
            # Load Parameters for each MU
            d4.diam, d4.L, d4.Ra, d4.cm, d4.gcabar_L_Ca_inact, d4.ghbar_gh, d4.g_pas, d4.e_pas, d4.half_gh, d4.htau_gh, d4.theta_m_L_Ca_inact, \
            d4.tau_m_L_Ca_inact, d4.theta_h_L_Ca_inact, d4.tau_h_L_Ca_inact, d4.kappa_h_L_Ca_inact = Set_Parameters_D4(self, self.no_mu, self.filepath, self.CaPIC, i) 

            # Load Cortical Signals
            h.cortical_signals_d4[i] = h.Vector(h.tstop)
            h.tvec_d4[i] = h.Vector(h.tstop) 
            for t in range(int(h.tstop)):
               h.cortical_signals_d4[i].x[t] = self.scaling_factor*(1/((np.pi*d4.diam*d4.L)*0.01))*(self.all_excitatory[i,t]*(d4(0.5).v - self.E_e) + self.all_inhibitory[i,t]*(d4(0.5).v - self.E_i) + self.all_correlated[i,t]*(d4(0.5).v - self.E_e))
               h.tvec_d4[i].x[t] = t                
            h.iclamp_d4[i] = h.IClamp(d4(0.5))
            h.iclamp_d4[i].dur = h.tstop
            h.iclamp_d4[i].delay = 0
            h.cortical_signals_d4[i].play(h.iclamp_d4[i], h.iclamp_d4[i]._ref_amp, h.tvec_d4[i], 1)
            self.updated_cortical_signals_d4.append(h.cortical_signals_d4[i].as_numpy())  

            self.all_d4.append(d4) 

    def __get_force_twitches__(self):                     
  
        for i in range(self.no_mu):
            ipi = np.zeros([len(self.st_all_realtime[i::self.no_mu][-1])])
            f = np.zeros([len(ipi), self.endt])             
            try:
                for k in range(len(self.st_all_realtime[i::self.no_mu][-1]) - 1): 
                    ipi[k] = self.st_all_realtime[i::self.no_mu][-1][k+1] - self.st_all_realtime[i::self.no_mu][-1][k] # Calculate IPI
                    sig = 1 - np.exp(-2*((self.T[i]/ipi[k])**3))       
                    norm_twitch = 1-np.exp(-2*((0.4)**3))
                    if self.T[i]/ipi[k] > 0.4:
                        g = sig/(self.T[i]/ipi[k])
                        g = g/norm_twitch
                    else:
                        g = 1                                    
                    for t in range(self.endt):
                        f[k,t] = (g*self.P[i]*(t/self.T[i]))*np.exp(1-(t/self.T[i]))  
                self.all_ipi.append(ipi)
                self.all_force_twitches.append(f)                                    
            except IndexError:
                print("No Fires")                  
    
    def __integrate__(self):

        # Calculate Total Pool Force
        self.all_ipi                = list()
        self.all_force_twitches     = list()
        self.All_Force              = np.zeros([self.no_mu, len(np.arange(0, self.durn, self.dt)) + 1 + self.endt])    
        self.Total_All_Force        = np.zeros([len(np.arange(0, self.durn, self.dt)) + 1 + self.endt])   
        self.Current_Error          = np.zeros([len(np.arange(0, self.durn, self.dt)) + 1 + self.endt])  
        self.all_Current_Error_Sum  = list()
       # self.Cumulative_Error       = np.zeros([len(np.arange(0, self.durn, self.dt)) + 1 + self.endt])  
        self.called                 = list()
        self.all_fr_rates           = list()
        self.all_fr2_rates          = list()
        self.all_err                = list()
        self.all_fr_pi_rates        = list()
        self.all_fr2_pi_rates       = list()
        self.all_err_pi             = list()
        self.new_excitatory_cells   = list()
        self.new_inhibitory_cells   = list()
        self.new_correlated_cells   = list()
        self.st_all_realtime        = list()
        self.all_temp_counters      = list()
        self.all_temp_values        = list()
                
        while h.t < h.tstop:
            
            for j in range(len(self.force_controller_call_times)):                    
                
                ## If it is time to call the controller (ever 150 ms)
                if np.round(h.t, decimals = 2) == self.force_controller_call_times[j]:
                    
                    ## Append all call times to list
                    self.called.append(self.force_controller_call_times[j])                     
                    
                    try:
                        
                        ## Make sure the controller isn't called twice for the same time period
                        if self.called[-1] != self.called[-2]:   
                          
                            ## Get the spike times from the somatic voltage
                            for i in range(self.no_mu):
                                st_realtime = []
                                for k in range(len(self.all_vsoma[i])-1):
                                    if self.all_vsoma[i][k] <= -20 and self.all_vsoma[i][k+1] > -20:
                                        st_realtime.append(self.trec[k])
                                self.st_all_realtime.append(st_realtime)                            
                            
                            ## Calculate IPIs and corresponding twitches
                            self.__get_force_twitches__() 
                                
                            ## Grab Force for 150ms and above
                            for t in range(int(self.force_controller_call_times[j-1]), int(self.force_controller_call_times[j])):
                                for i in range(self.no_mu):
                                    for counter, value in enumerate(self.st_all_realtime[i + self.no_mu*(j)]):                                              
                                        if int(np.ceil(value)) == t:
                                            self.All_Force[i, t:(t + 500)] = self.All_Force[i, t:(t + 500)] + self.all_force_twitches[i + self.no_mu*(j)][counter]     
     
                            ## Feedback Implementation
                            if self.pk != 1.0:
  
                                if self.force_controller_call_times[j] < (self.cornerb/2 - 0.5)/self.dt:
                                    
                                    ## Calculate Error and Update FR/FR2 Parameters
                                    curr_call = int(self.force_controller_call_times[j])
                                    prev_call = int(self.force_controller_call_times[j-1])
                                    midpoint = int(0.5*self.force_controller_call_times[0])
                                    
                                    self.err = (np.mean(sum(self.All_Force[:, prev_call:curr_call])) - (self.target[prev_call + midpoint]*self.Fmax))/np.mean(sum(self.All_Force[:, prev_call:curr_call])) 
                                    self.all_err.append(self.err)
                                    
                                    self.T_int = (0.75*self.pk*10)
                                    self.errmod = float(sys.argv[2])
                                    if int(self.force_controller_call_times[j]) >= 650:
                                        self.pi_fxn = self.errmod*(self.err + (1/self.T_int)*sum(self.all_err[j-3:j]))
                                    else:
                                        self.pi_fxn = self.errmod*(self.err + (1/self.T_int)*sum(self.all_err))  
                                    self.all_err_pi.append(self.pi_fxn)

                                    self.fr_pi = self.fr_pi - self.fr_pi*(self.pi_fxn)
                                    self.fr2_pi = self.fr_pi*3.1
                                    self.all_fr_pi_rates.append(self.fr_pi)
                                    self.all_fr2_pi_rates.append(self.fr2_pi)
                                    
                                    self.new_excitatory_cells = Excitatory_Cell_Generation_Update(self, k = 2.0, fr2 = self.fr2_pi, dt = self.dt, \
                                                                                    no_mu = self.no_mu, total_no_input_ind = 75, durn = self.durn, max_syn = 10, a_s = self.a_s, start = self.force_controller_call_times[j], \
                                                                                    endtime = self.force_controller_call_times[j+1], EPSP = self.EPSP)
                                   
                                    self.new_inhibitory_cells = Inhibitory_Cell_Generation_Update(self, k = 2.0, fr2 = self.fr2_pi, dt = self.dt, \
                                                                                    no_mu = self.no_mu, total_no_input_inhib = 25, durn = self.durn, max_syn = 10, a_s_inhib = self.a_s_inhib, \
                                                                                    start = self.force_controller_call_times[j], endtime = self.force_controller_call_times[j+1], EPSP = self.EPSP)
                                   
                                    self.new_correlated_cells = Correlated_Cell_Generation_Update(self, no_mu = self.no_mu, filepath = self.filepath, durn = self.durn, dt = self.dt, A = self.fr_pi, B = self.B, \
                                                                                cortical_sig_amp = self.cortical_sig_amp, pk = self.pk, start = self.force_controller_call_times[j], \
                                                                            endtime = self.force_controller_call_times[j+1], EPSP_CT = self.EPSP_CT, EPSP_CT_2 = self.EPSP_CT_2, controller_call_time = self.force_controller_call_times[j])

                                    # Update MN Inputs
                                    for i in range(self.no_mu):
                                        
                                        curr_call = int(self.force_controller_call_times[j])
                                        next_call = int(self.force_controller_call_times[j+1])
                                                       
                                        excitatory_current = self.new_excitatory_cells[i][curr_call:next_call]
                                        inhibitory_current = self.new_inhibitory_cells[i][curr_call:next_call]
                                        correlated_current = self.new_correlated_cells[i][curr_call:next_call]
                                        
                                        current_density_scaling_d1 = 1/((np.pi*self.all_d1[i].diam*self.all_d1[i].L)*0.01)
                                        current_density_scaling_d2 = 1/((np.pi*self.all_d2[i].diam*self.all_d2[i].L)*0.01)
                                        current_density_scaling_d3 = 1/((np.pi*self.all_d3[i].diam*self.all_d3[i].L)*0.01)
                                        current_density_scaling_d4 = 1/((np.pi*self.all_d4[i].diam*self.all_d4[i].L)*0.01)

                                        
                                        self.updated_cortical_signals_d1[i][curr_call:next_call] = self.scaling_factor*(current_density_scaling_d1)*\
                                        (excitatory_current*(self.all_d1[i](0.5).v - self.E_e) + inhibitory_current*(self.all_d1[i](0.5).v - self.E_i) + correlated_current*(self.all_d1[i](0.5).v - self.E_e))
                                        
                                        self.updated_cortical_signals_d2[i][curr_call:next_call] = self.scaling_factor*(current_density_scaling_d2)*\
                                        (excitatory_current*(self.all_d2[i](0.5).v - self.E_e) + inhibitory_current*(self.all_d2[i](0.5).v - self.E_i) + correlated_current*(self.all_d2[i](0.5).v - self.E_e))
                                        
                                        self.updated_cortical_signals_d3[i][curr_call:next_call] = self.scaling_factor*(current_density_scaling_d3)*\
                                        (excitatory_current*(self.all_d3[i](0.5).v - self.E_e) + inhibitory_current*(self.all_d3[i](0.5).v - self.E_i) + correlated_current*(self.all_d3[i](0.5).v - self.E_e))
                                        
                                        self.updated_cortical_signals_d4[i][curr_call:next_call] = self.scaling_factor*(current_density_scaling_d4)*\
                                        (excitatory_current*(self.all_d4[i](0.5).v - self.E_e) + inhibitory_current*(self.all_d4[i](0.5).v - self.E_i) + correlated_current*(self.all_d4[i](0.5).v - self.E_e))

                                
                                if self.force_controller_call_times[j] >= (self.cornerb/2 - 0.5)/self.dt:
                                
                                    ## Calculate Error and Update FR/FR2 Parameters
                                    curr_call = int(self.force_controller_call_times[j])
                                    prev_call = int(self.force_controller_call_times[j-1])
                                    midpoint = int(0.5*self.force_controller_call_times[0])
                                    
                                    self.err = (np.mean(sum(self.All_Force[:, prev_call:curr_call])) - (self.target[prev_call + midpoint]*self.Fmax))/np.mean(sum(self.All_Force[:, prev_call:curr_call])) 
                                    self.all_err.append(self.err)
                                    
                                    self.T_int = (0.75*self.pk*10)
                                    self.errmod = float(sys.argv[2])
                                    if int(self.force_controller_call_times[j]) >= 650:
                                        self.pi_fxn = self.errmod*(self.err + (1/self.T_int)*sum(self.all_err[j-3:j]))
                                    else:
                                        self.pi_fxn = self.errmod*(self.err + (1/self.T_int)*sum(self.all_err))  
                                    self.all_err_pi.append(self.pi_fxn)

                                    self.fr_pi = self.fr_pi - self.fr_pi*(self.pi_fxn)
                                    self.fr2_pi = self.fr_pi*3.1
                                    self.all_fr_pi_rates.append(self.fr_pi)
                                    self.all_fr2_pi_rates.append(self.fr2_pi)

                                    self.new_excitatory_cells = Excitatory_Cell_Generation_Update(self, k = 2.0, fr2 = self.fr2_pi, dt = self.dt, \
                                                                                    no_mu = self.no_mu, total_no_input_ind = 75, durn = self.durn, max_syn = 10, a_s = self.a_s, start = self.force_controller_call_times[j], \
                                                                                    endtime = self.force_controller_call_times[j+1], EPSP = self.EPSP)
                                   
                                    self.new_inhibitory_cells = Inhibitory_Cell_Generation_Update(self, k = 2.0, fr2 = self.fr2_pi, dt = self.dt, \
                                                                                    no_mu = self.no_mu, total_no_input_inhib = 25, durn = self.durn, max_syn = 10, a_s_inhib = self.a_s_inhib, \
                                                                                    start = self.force_controller_call_times[j], endtime = self.force_controller_call_times[j+1], EPSP = self.EPSP)
                                   
                                    self.new_correlated_cells = Correlated_Cell_Generation_Update(self, no_mu = self.no_mu, filepath = self.filepath, durn = self.durn, dt = self.dt, A = self.fr_pi, B = self.B, \
                                                                                cortical_sig_amp = self.cortical_sig_amp, pk = self.pk, start = self.force_controller_call_times[j], \
                                                                                endtime = self.force_controller_call_times[j+1], EPSP_CT = self.EPSP_CT, EPSP_CT_2 = self.EPSP_CT_2, controller_call_time = self.force_controller_call_times[j])

                                    # Update MN Inputs
                                    for i in range(self.no_mu):
                                        
                                        curr_call = int(self.force_controller_call_times[j])
                                        next_call = int(self.force_controller_call_times[j+1])
                                                       
                                        excitatory_current = self.new_excitatory_cells[i][curr_call:next_call]
                                        inhibitory_current = self.new_inhibitory_cells[i][curr_call:next_call]
                                        correlated_current = self.new_correlated_cells[i][curr_call:next_call]
                                        
                                        current_density_scaling_d1 = 1/((np.pi*self.all_d1[i].diam*self.all_d1[i].L)*0.01)
                                        current_density_scaling_d2 = 1/((np.pi*self.all_d2[i].diam*self.all_d2[i].L)*0.01)
                                        current_density_scaling_d3 = 1/((np.pi*self.all_d3[i].diam*self.all_d3[i].L)*0.01)
                                        current_density_scaling_d4 = 1/((np.pi*self.all_d4[i].diam*self.all_d4[i].L)*0.01)

                                        
                                        self.updated_cortical_signals_d1[i][curr_call:next_call] = self.scaling_factor*(current_density_scaling_d1)*\
                                        (excitatory_current*(self.all_d1[i](0.5).v - self.E_e) + inhibitory_current*(self.all_d1[i](0.5).v - self.E_i) + correlated_current*(self.all_d1[i](0.5).v - self.E_e))
                                        
                                        self.updated_cortical_signals_d2[i][curr_call:next_call] = self.scaling_factor*(current_density_scaling_d2)*\
                                        (excitatory_current*(self.all_d2[i](0.5).v - self.E_e) + inhibitory_current*(self.all_d2[i](0.5).v - self.E_i) + correlated_current*(self.all_d2[i](0.5).v - self.E_e))
                                        
                                        self.updated_cortical_signals_d3[i][curr_call:next_call] = self.scaling_factor*(current_density_scaling_d3)*\
                                        (excitatory_current*(self.all_d3[i](0.5).v - self.E_e) + inhibitory_current*(self.all_d3[i](0.5).v - self.E_i) + correlated_current*(self.all_d3[i](0.5).v - self.E_e))
                                        
                                        self.updated_cortical_signals_d4[i][curr_call:next_call] = self.scaling_factor*(current_density_scaling_d4)*\
                                        (excitatory_current*(self.all_d4[i](0.5).v - self.E_e) + inhibitory_current*(self.all_d4[i](0.5).v - self.E_i) + correlated_current*(self.all_d4[i](0.5).v - self.E_e))

                    except IndexError:
                    
                        if self.force_controller_call_times[j] == self.force_controller_call_times[0]:

                            ## Get the spike times from the somatic voltage
                            for i in range(self.no_mu):
                                st_realtime = []
                                for k in range(len(self.all_vsoma[i])-1):
                                    if self.all_vsoma[i][k] <= -20 and self.all_vsoma[i][k+1] > -20:
                                        st_realtime.append(self.trec[k])
                                self.st_all_realtime.append(st_realtime)                            
                                
                            ## Calculate IPIs and corresponding twitches
                            self.__get_force_twitches__() 
                            
                            ## Grab Force for the first 150 ms
                            for t in range(int(self.force_controller_call_times[j])):
                                for i in range(self.no_mu):
                                    for counter, value in enumerate(self.st_all_realtime[i + self.no_mu*(j)]):
                                        if int(np.ceil(value)) == t:   
                                            self.All_Force[i, t:(t + 500)] = self.All_Force[i, t:(t + 500)] + self.all_force_twitches[i + self.no_mu*(j)][counter]
       
                            ## Feedback Implementation
                            if self.pk != 1.0:
                                
                                ## Calculate Error and Update FR/FR2 Parameters
                                curr_call = int(self.force_controller_call_times[j])
                                prev_call = int(self.force_controller_call_times[j-1])
                                midpoint = int(0.5*self.force_controller_call_times[0])
                                
                                self.err = (np.mean(sum(self.All_Force[:, 0:curr_call])) - (self.target[prev_call + midpoint]*self.Fmax))/np.mean(sum(self.All_Force[:, 0:curr_call])) 
                                self.all_err.append(self.err)
                                
                                self.T_int = (0.75*self.pk*10)
                                self.errmod = float(sys.argv[2])
                                if int(self.force_controller_call_times[j]) >= 650:
                                    self.pi_fxn = self.errmod*(self.err + (1/self.T_int)*sum(self.all_err[j-3:j]))
                                else:
                                    self.pi_fxn = self.errmod*(self.err + (1/self.T_int)*sum(self.all_err))  
                                self.all_err_pi.append(self.pi_fxn)

                                self.fr_pi = self.fr_pi - self.fr_pi*(self.pi_fxn)
                                self.fr2_pi = self.fr_pi*3.1
                                self.all_fr_pi_rates.append(self.fr_pi)
                                self.all_fr2_pi_rates.append(self.fr2_pi)
                                    
                                self.new_excitatory_cells = Excitatory_Cell_Generation_Update(self, k = 2.0, fr2 = self.fr2_pi, dt = self.dt, \
                                                                                no_mu = self.no_mu, total_no_input_ind = 75, durn = self.durn, max_syn = 10, a_s = self.a_s, start = self.force_controller_call_times[j], \
                                                                                endtime = self.force_controller_call_times[j+1], EPSP = self.EPSP)
                               
                                self.new_inhibitory_cells = Inhibitory_Cell_Generation_Update(self, k = 2.0, fr2 = self.fr2_pi, dt = self.dt, \
                                                                                no_mu = self.no_mu, total_no_input_inhib = 25, durn = self.durn, max_syn = 10, a_s_inhib = self.a_s_inhib, \
                                                                                start = self.force_controller_call_times[j], endtime = self.force_controller_call_times[j+1], EPSP = self.EPSP)
                               
                                self.new_correlated_cells = Correlated_Cell_Generation_Update(self, no_mu = self.no_mu, filepath = self.filepath, durn = self.durn, dt = self.dt, A = self.fr_pi, B = self.B, \
                                                                            cortical_sig_amp = self.cortical_sig_amp, pk = self.pk, start = self.force_controller_call_times[j], \
                                                                            endtime = self.force_controller_call_times[j+1], EPSP_CT = self.EPSP_CT, EPSP_CT_2 = self.EPSP_CT_2, controller_call_time = self.force_controller_call_times[j])

                                # Update MN Inputs
                                for i in range(self.no_mu):
                                    
                                    curr_call = int(self.force_controller_call_times[j])
                                    next_call = int(self.force_controller_call_times[j+1])
                                                   
                                    excitatory_current = self.new_excitatory_cells[i][curr_call:next_call]
                                    inhibitory_current = self.new_inhibitory_cells[i][curr_call:next_call]
                                    correlated_current = self.new_correlated_cells[i][curr_call:next_call]
                                    
                                    current_density_scaling_d1 = 1/((np.pi*self.all_d1[i].diam*self.all_d1[i].L)*0.01)
                                    current_density_scaling_d2 = 1/((np.pi*self.all_d2[i].diam*self.all_d2[i].L)*0.01)
                                    current_density_scaling_d3 = 1/((np.pi*self.all_d3[i].diam*self.all_d3[i].L)*0.01)
                                    current_density_scaling_d4 = 1/((np.pi*self.all_d4[i].diam*self.all_d4[i].L)*0.01)

                                    
                                    self.updated_cortical_signals_d1[i][curr_call:next_call] = self.scaling_factor*(current_density_scaling_d1)*\
                                    (excitatory_current*(self.all_d1[i](0.5).v - self.E_e) + inhibitory_current*(self.all_d1[i](0.5).v - self.E_i) + correlated_current*(self.all_d1[i](0.5).v - self.E_e))
                                    
                                    self.updated_cortical_signals_d2[i][curr_call:next_call] = self.scaling_factor*(current_density_scaling_d2)*\
                                    (excitatory_current*(self.all_d2[i](0.5).v - self.E_e) + inhibitory_current*(self.all_d2[i](0.5).v - self.E_i) + correlated_current*(self.all_d2[i](0.5).v - self.E_e))
                                    
                                    self.updated_cortical_signals_d3[i][curr_call:next_call] = self.scaling_factor*(current_density_scaling_d3)*\
                                    (excitatory_current*(self.all_d3[i](0.5).v - self.E_e) + inhibitory_current*(self.all_d3[i](0.5).v - self.E_i) + correlated_current*(self.all_d3[i](0.5).v - self.E_e))
                                    
                                    self.updated_cortical_signals_d4[i][curr_call:next_call] = self.scaling_factor*(current_density_scaling_d4)*\
                                    (excitatory_current*(self.all_d4[i](0.5).v - self.E_e) + inhibitory_current*(self.all_d4[i](0.5).v - self.E_i) + correlated_current*(self.all_d4[i](0.5).v - self.E_e))

                                                                                                              
                        else:
                            continue
                    
            h.advance()
                                           
    def __go__(self):
        
        # Set Resting Potential and Run Control
        h.finitialize(-65)
        self.__integrate__()  
        
    def __main__(self):   
        
        ## Connectivity Parameters
        Connectivity_Dict = sio.loadmat('InputConnectionDirect_15pc_600_100.mat')
        InputConnection = np.array(Connectivity_Dict['InputConnection'])
        total_no_input = np.amax(InputConnection)                                  # total number of correlated inputs to each MN 

        ## Correlated Cortical Input Generation
        t_x = np.arange(0, self.tmax_CI + 1, self.EPSP_dt)
        gal = np.zeros([np.size(t_x)])
        galp = self.tau_CI/np.exp(1)
        tr = t_x[int(round(self.ts/self.EPSP_dt)-1):len(t_x)] - (self.ts - self.EPSP_dt)
        gal[int(round(self.ts/self.EPSP_dt)-1):len(t_x)] = (tr + self.d_n)*np.exp((-(tr + self.d_n))/self.tau_CI)/galp
        self.EPSP_CT = gal
        
        t_x2 = np.arange(0, self.tmax_II + 1, self.EPSP_dt)
        gal2 = np.zeros([np.size(t_x2)])
        galp2 = self.tau_II/np.exp(1)
        tr2 = t_x2[int(round(self.ts/self.EPSP_dt)-1):len(t_x2)]-(self.ts -self.EPSP_dt)
        gal2[int(round(self.ts/self.EPSP_dt)-1):len(t_x2)] = (tr2 + self.d_n)*np.exp((-(tr2 + self.d_n))/self.tau_II)/galp2
        self.EPSP_CT_2 = gal2
        self.EPSP_CT_2 = self.a_ct*self.EPSP_CT_2
        
        pulse_train = np.zeros([len(np.arange(0, self.durn + self.dt, self.dt)), total_no_input])
        pulse_train_total = np.zeros([total_no_input, len(np.arange(0, self.durn + self.dt, self.dt)) + len(self.EPSP_CT)])
        temp_pulse_CT = np.zeros([self.no_mu, len(np.arange(0, self.durn + self.dt, self.dt))])                 # Common Modulatory Pulse Train

        ## Independent Excitatory and Inhibitory Input Generation
        t_x2 = np.arange(0, self.tmax_II + self.EPSP_dt, self.EPSP_dt)
        gal2 = np.zeros([np.size(t_x2)])
        galp2 = self.tau_II/np.exp(1)
        tr2 = t_x2[int(round(self.ts/self.EPSP_dt)-1):len(t_x2)]-(self.ts - self.EPSP_dt)
        gal2[int(round(self.ts/self.EPSP_dt)-1):len(t_x2)] = (tr2 + self.d_n)*np.exp((-(tr2 + self.d_n))/self.tau_II)/galp2
        self.EPSP = gal2
        
        pulse_train_ind = np.zeros([self.no_mu, self.total_no_input_ind, len(np.arange(0, self.durn + self.dt, self.dt))])
        pulse_train_ind_ip = np.zeros([self.no_mu, self.total_no_input_ind, len(np.arange(0, self.durn + self.dt, self.dt)) + len(self.EPSP)])
        pulse_count_ind = np.zeros([self.no_mu, self.total_no_input_ind]) 
        temp_pulse_ex = np.zeros([self.no_mu, len(np.arange(0, self.durn + self.dt, self.dt))])
        pulse_train_inhib = np.zeros([self.no_mu, self.total_no_input_inhib, len(np.arange(0, self.durn + self.dt, self.dt))])
        pulse_train_inhib_ip = np.zeros([self.no_mu, self.total_no_input_inhib, len(np.arange(0, self.durn + self.dt, self.dt)) + len(self.EPSP)])
        pulse_count_inhib = np.zeros([self.no_mu, self.total_no_input_inhib]) 
        temp_pulse_inhib = np.zeros([self.no_mu, len(np.arange(0, self.durn + self.dt, self.dt))])
        
        ## Set Up Initial Cortical Signals      
        
        self.all_excitatory, self.all_pulse_train_ind, self.all_pulse_count_ind, self.all_pulse_train_ind_ip = Excitatory_Cell_Generation(self, self.k, self.fr2_pi, self.dt, self.no_mu, self.total_no_input_ind, \
                     self.durn, self.max_syn, self.a_s, self.start, self.endtime, self.EPSP, pulse_train_ind, pulse_train_ind_ip, pulse_count_ind, temp_pulse_ex)    
                     
        self.all_inhibitory, self.all_pulse_train_inhib, self.all_pulse_count_inhib, self.all_pulse_train_inhib_ip = Inhibitory_Cell_Generation(self, self.k, self.fr2_pi, self.dt, self.no_mu, self.total_no_input_inhib, \
                        self.durn, self.max_syn, self.a_s_inhib, self.start, self.endtime, self.EPSP, pulse_train_inhib, pulse_train_inhib_ip, pulse_count_inhib, temp_pulse_inhib) 
                        
        self.all_correlated, self.all_pulse_train_total, self.all_pulse_train = Correlated_Cell_Generation(self, self.no_mu, self.filepath, self.durn, self.dt, self.fr_pi, self.B, \
                     self.cortical_sig_amp, self.pk, self.start, self.endtime, self.EPSP_CT, self.EPSP_CT_2, pulse_train, pulse_train_total, temp_pulse_CT)
   
        
        ## Create Sections and Set Parameters for MUs

        self.__create__()    
        
        ## Set up Voltage Recording Vectors    
        self.all_vd1 = list()
        self.all_vd2 = list()
        self.all_vd3 = list()
        self.all_vd4 = list()
        self.all_vsoma = list()
        self.all_PIC1_amp = list()
        self.all_PIC2_amp = list()
        self.all_PIC3_amp = list()
        self.all_PIC4_amp = list()
        self.all_PIC1_m = list()
        self.all_PIC2_m = list()
        self.all_PIC3_m = list()
        self.all_PIC4_m = list()
        self.all_gh_d1 = list()
        self.all_gh_d2 = list()
        self.all_gh_d3 = list()
        self.all_gh_d4 = list()
        self.all_gh_soma = list()
        self.all_kdrRL_soma = list()
        self.all_mAHP_soma = list()
        self.all_na3rp_soma = list()
        self.all_naps_soma = list()
        
        for i in range(self.no_mu):
            vd1 = h.Vector()
            vd1.record(self.all_d1[i](0.5)._ref_v)
            self.all_vd1.append(vd1)
            vd2 = h.Vector()
            vd2.record(self.all_d2[i](0.5)._ref_v)
            self.all_vd2.append(vd2)
            vd3 = h.Vector()
            vd3.record(self.all_d3[i](0.5)._ref_v)
            self.all_vd3.append(vd3)
            vd4 = h.Vector()
            vd4.record(self.all_d4[i](0.5)._ref_v)
            self.all_vd4.append(vd4)
            vsoma = h.Vector()
            vsoma.record(self.all_soma[i](0.5)._ref_v)
            self.all_vsoma.append(vsoma)
            
            PIC1_amp = h.Vector()
            PIC1_amp.record(self.all_d1[i](0.5)._ref_icaL_L_Ca_inact)
            self.all_PIC1_amp.append(PIC1_amp)
            PIC1_m = h.Vector()
            PIC1_m.record(self.all_d1[i](0.5)._ref_m_L_Ca_inact)
            self.all_PIC1_m.append(PIC1_m)
            
            PIC2_amp = h.Vector()
            PIC2_amp.record(self.all_d2[i](0.5)._ref_icaL_L_Ca_inact)
            self.all_PIC2_amp.append(PIC2_amp)
            PIC2_m = h.Vector()
            PIC2_m.record(self.all_d2[i](0.5)._ref_m_L_Ca_inact)
            self.all_PIC2_m.append(PIC2_m)
            
            PIC3_amp = h.Vector()
            PIC3_amp.record(self.all_d3[i](0.5)._ref_icaL_L_Ca_inact)
            self.all_PIC3_amp.append(PIC3_amp)
            PIC3_m = h.Vector()
            PIC3_m.record(self.all_d3[i](0.5)._ref_m_L_Ca_inact)
            self.all_PIC3_m.append(PIC3_m)
            
            PIC4_amp = h.Vector()
            PIC4_amp.record(self.all_d4[i](0.5)._ref_icaL_L_Ca_inact)
            self.all_PIC4_amp.append(PIC4_amp)
            PIC4_m = h.Vector()
            PIC4_m.record(self.all_d4[i](0.5)._ref_m_L_Ca_inact)
            self.all_PIC4_m.append(PIC4_m)
        
            # gh_d1 = h.Vector()
            # gh_d2 = h.Vector()
            # gh_d3 = h.Vector()
            # gh_d4 = h.Vector()
            # gh_soma = h.Vector()
            # kdrRL_soma = h.Vector()
            # mAHP_soma = h.Vector()
            # na3rp_soma = h.Vector()
            # naps_soma = h.Vector()
            
            # gh_d1.record(self.all_d1[i](0.5)._ref_gh)
            # gh_d2.record(self.all_d2[i](0.5)._ref_gh)
            # gh_d3.record(self.all_d3[i](0.5)._ref_gh)
            # gh_d4.record(self.all_d4[i](0.5)._ref_gh)
            # gh_soma.record(self.all_soma[i](0.5)._ref_gh)
            # kdrRL_soma.record(self.all_soma[i](0.5)._ref_kdrRL)
            # mAHP_soma.record(self.all_soma[i](0.5)._ref_mAHP)
            # na3rp_soma.record(self.all_soma[i](0.5)._ref_na3rp)
            # naps_soma.record(self.all_soma[i](0.5)._ref_naps)
            
            # self.all_gh_d1.append(gh_d1)
            # self.all_gh_d2.append(gh_d2)
            # self.all_gh_d3.append(gh_d3)
            # self.all_gh_d4.append(gh_d4)
            # self.all_gh_soma.append(gh_soma)
            # self.all_kdrRL_soma.append(kdrRL_soma)
            # self.all_mAHP_soma.append(mAHP_soma)
            # self.all_na3rp_soma.append(na3rp_soma)
            # self.all_naps_soma.append(naps_soma)
        
        ## Set up Force Trajectory for Visual Feedback Simulation
        # self.T = T_Distribution(self)
        # self.P = P_Distribution(self)
        
        self.P, self.T = Get_P_T_Distributions(self)
        if self.pk != 1.0:
            
            try:
            
                self.cornera = 0                                         
                self.cornerb = 0.75*(self.pk*10)
                self.cornerc = 0.75*(self.pk*10) + self.ss_length
                self.cornerd = 0.75*(self.pk*10*2) + self.ss_length
                self.target = np.zeros([len(np.arange(0, self.durn, self.dt))])
                self.target[int(self.cornera/self.dt):int(self.cornerb/self.dt)] = np.arange(self.pk/((self.cornerb-self.cornera)/self.dt), self.pk, self.pk/((self.cornerb-self.cornera)/self.dt))
                self.target[int(self.cornerb/self.dt):int(self.cornerc/self.dt)] = np.ones([1, len(np.arange(self.cornerb/self.dt, self.cornerc/self.dt, 1))])*self.pk   
                self.target[int(self.cornerc/self.dt):int(self.cornerd/self.dt)] = np.arange(self.pk, 0, -self.pk/((self.cornerd-self.cornerc)/self.dt))       
                
            except ValueError:
            
                self.cornera = 0                                         
                self.cornerb = 0.75*(self.pk*10)
                self.cornerc = 0.75*(self.pk*10) + self.ss_length
                self.cornerd = 0.75*(self.pk*10*2) + self.ss_length
                self.target = np.zeros([len(np.arange(0, self.durn, self.dt))])
                self.target[int(self.cornera/self.dt):int(self.cornerb/self.dt)-1] = np.arange(self.pk/((self.cornerb-self.cornera)/self.dt), self.pk, self.pk/((self.cornerb-self.cornera)/self.dt))
                self.target[int(self.cornerb/self.dt):int(self.cornerc/self.dt)] = np.ones([1, len(np.arange(self.cornerb/self.dt, self.cornerc/self.dt, 1))])*self.pk   
                self.target[int(self.cornerc/self.dt):int(self.cornerd/self.dt)] = np.arange(self.pk, 0, -self.pk/((self.cornerd-self.cornerc)/self.dt))    
                
        else:                                                
            self.fr = self.max_fr
            self.fr2 = self.max_fr2 
        
        # Set Up Time Array to Call Controller
        self.force_controller_call_times = np.arange(150.0, h.tstop, 150.0)    
    
        self.trec = h.Vector()
        self.trec.record(h._ref_t)
    
        ## Run the Model
        self.__go__()
        
        self.st_all = list()
        for i in range(self.no_mu):
            st = []
            for j in range(len(self.all_vsoma[i])-1):
                if self.all_vsoma[i][j] <= -20 and self.all_vsoma[i][j+1] > -20:
                    st.append(self.trec[j])
            self.st_all.append(st)   
        self.Total_All_Force = sum(self.All_Force)
        
        # Output data folder directory label
        output_data_folder_path_1 = "errmod={:.6f}".format(self.errmod)   

        myData_1 = sio.savemat('/scratch/16201854/err_'+output_data_folder_path_1+'_100MU_FB_cortical_sigs', {'cd1': self.updated_cortical_signals_d1, \
        'cd2': self.updated_cortical_signals_d2,'cd3': self.updated_cortical_signals_d3,'cd4': self.updated_cortical_signals_d4})
        myData_2 = sio.savemat('/scratch/16201854/err_'+output_data_folder_path_1+'_100MU_FB_spikes', {'st': self.st_all, 'vsoma': self.all_vsoma, 'st_realtime': self.st_all_realtime})
        # myData_3 = sio.savemat('/scratch/16201854/err_'+output_data_folder_path_1+'_100MU_FB_force', {'Total_Force': self.Total_All_Force, 'Force': self.All_Force, 'fr': self.all_fr_rates, 'fr2': self.all_fr2_rates, 'err': self.all_err, \
        # 'Current_Error': self.Current_Error, 'Current_Error_Sum': self.Current_Error_Sum, 'PI_Fxn': self.all_err_pi, 'fr_pi': self.fr_pi, 'fr2_pi': self.fr2_pi})
        myData_3 = sio.savemat('/scratch/16201854/err_'+output_data_folder_path_1+'_100MU_FB_force', {'Total_Force': self.Total_All_Force, 'Force': self.All_Force, 'err': self.all_err, \
        'Current_Error': self.Current_Error, 'Current_Error_Sum': self.all_Current_Error_Sum, 'PI_Fxn': self.all_err_pi, 'fr_pi': self.all_fr_pi_rates, 'fr2_pi': self.all_fr2_pi_rates})
        myData_4 = sio.savemat('/scratch/16201854/err_'+output_data_folder_path_1+'_100MU_FB_PICs', {'PIC_1_amp': self.all_PIC1_amp, 'PIC_1_m': self.all_PIC1_m, 'PIC_2_amp': self.all_PIC2_amp, 'PIC_2_m': self.all_PIC2_m, \
        'PIC_3_amp': self.all_PIC3_amp, 'PIC_3_m': self.all_PIC3_m, 'PIC_4_amp': self.all_PIC4_amp, 'PIC_4_m': self.all_PIC4_m})
        # myData_6 = sio.savemat('/scratch/16201854/pk_'+output_data_folder_path_1+'_100MU_FB_Conductances_NoPICs', {'gh_d1': self.all_gh_d1, 'gh_d2': self.all_gh_d2, \
        # 'gh_d3': self.all_gh_d3, 'gh_d4': self.all_gh_d4, 'gh_soma': self.all_gh_soma, 'kdrRL_soma': self.all_kdrRL_soma, 'mAHP_soma': self.all_mAHP_soma, \
        # 'na3rp_soma': self.all_na3rp_soma, 'naps_soma': self.all_naps_soma})

        return myData_1, myData_2, myData_3, myData_4

        
if __name__ == '__main__':
    runner = Run_Neuromuscular_Model()
    runner.__main__()
                